class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.references :course, index: true, foreign_key: true
      t.references :instructor, index: true, foreign_key: true
      t.integer :code
      t.integer :capacity
      t.integer :enrolled
      t.integer :wait_listed
      t.boolean :open_for_enrollment, default: false

      t.timestamps null: false
    end
  end
end
