# == Schema Information
#
# Table name: rooms
#
#  id          :integer          not null, primary key
#  number      :string
#  building_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class RoomsController < ApplicationController

  def show
    @room = Room.find(params[:id])
  end
end
