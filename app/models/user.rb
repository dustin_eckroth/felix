# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  code            :integer
#  first_name      :string
#  last_name       :string
#  email           :string
#  password_digest :string
#  admin           :boolean          default(FALSE)
#  student         :boolean          default(FALSE)
#  employee        :boolean          default(FALSE)
#  instructor      :boolean          default(FALSE)
#  total_credits   :integer
#  gpa             :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ActiveRecord::Base
  validates :last_name,  presence: true, length: { maximum: 50 }
  validates :code, uniqueness: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 },
                       allow_nil: true
                       
  has_many :enrollments
  has_many :sections, through: :enrollments
                       
  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  def interested_sections
    self.sections.where("status = ?", 1)
  end
  
  def wait_listed_sections
    self.sections.where("status = ?", 2)
  end
  
  def enrolled_sections
    self.sections.where("status = ?", 3)
  end
  
  private
  
    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end
  
end
