# == Schema Information
#
# Table name: courses
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  prefix      :string
#  code        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  credits     :integer
#

class Course < ActiveRecord::Base
  validates :name,  presence: true
  validates :description, presence: true
  validates :prefix, presence: true, length: { maximum: 4 }
  validates :code, presence: true, uniqueness: { scope: :prefix }
  validates :credits, presence: true, numericality: { only_integer: true }
  
  has_many :sections
  
  def abbr
    "#{self.prefix} #{self.code}"
  end
  

  def self.search(search)
    if search
      where("(name LIKE ?) OR (code LIKE ?) OR (prefix LIKE ?)", 
      "%#{search}%", "%#{search}%", "%#{search}%")
    else
      all
    end
  end


  
  #validates :employee_id, uniqueness: { scope: :area_id }
  #validates_uniqueness_of :employee_id, :scope => :area_id
  #validates :zipcode, :uniqueness => {:scope => [:recorded_at, :something_else]}
end
