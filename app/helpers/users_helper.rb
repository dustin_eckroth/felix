# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  code            :integer
#  first_name      :string
#  last_name       :string
#  email           :string
#  password_digest :string
#  admin           :boolean          default(FALSE)
#  student         :boolean          default(FALSE)
#  employee        :boolean          default(FALSE)
#  instructor      :boolean          default(FALSE)
#  total_credits   :integer
#  gpa             :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

module UsersHelper
  
  def user_schedule_html_class
    return "panel-collapse collapse in" if params[:date]
    return "panel-collapse collapse"
  end
end
