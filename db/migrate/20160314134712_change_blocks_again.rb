
# Table name: blocks
#
#  id         :integer          not null, primary key
#  section_id :integer
#  start_date :date
#  end_date   :date
#  start_time :time
#  end_time   :time
#  monday     :boolean
#  tuesday    :boolean
#  wednesday  :boolean
#  thursday   :boolean
#  friday     :boolean
#  saturday   :boolean
#  sunday     :boolean
#  room_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ChangeBlocksAgain < ActiveRecord::Migration
  def change
    change_column :blocks, :start_time, :string
    change_column :blocks, :end_time,   :string
  end
end
