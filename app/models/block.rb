# == Schema Information
#
# Table name: blocks
#
#  id         :integer          not null, primary key
#  section_id :integer
#  start_date :date
#  end_date   :date
#  start_time :string
#  end_time   :string
#  room_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  day        :integer
#

class Block < ActiveRecord::Base
  belongs_to :section
  belongs_to :room
  
  validate :block_is_unique
  
  # Returns a string with the start time and end time; e.g. "12:00-12:50"
  def time_range
    "#{self.start_time.to_time.strftime("%I:%M %p")}-#{self.end_time.to_time.strftime("%I:%M %p")}"
  end
  
  def length
    ((b.end_time - b.start_time)/100).to_i
  end
  
  def day_matches(date_to_compare)
    str_to_compare = "UMTWRFS"[date_to_compare]
    self.days.include?(str_to_compare)
  end
  
  # Returns true if the Block falls inside the scheduled calendar block
  def is_scheduled(date, hour)
    return false if !(self.day_matches(date.wday))    # Day of week matches block day
    return false if !(self.start_time.to_time.hour <= hour)   # Block hour matches calendar hour
    return false if !(self.end_time.to_time.hour >= hour)     # Block hour matches calendar hour
    return false if !(self.start_date <= date)        # Start date is after current day
    return false if !(self.end_date >= date)          # End date is before current day
    return true
  end
  
  # Returns the Building abbreviation and room number
  def location
    "#{self.room.building.abbr} #{self.room.number}"
  end
  
  private
  
    # Custom validations
    
    # Ensure that a new Block can't be created if it overlaps an existing one
    #  unless it belongs to the same Course
    # => e.g. Course A has a TTh lecture at 12:00 pm and three Sections that
    # =>        meet together in the same room at the same time
    def block_is_unique
      ## TODO : finish
    end
  
end
