class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.text :description
      t.string :prefix
      t.string :code
      t.integer :credits

      t.timestamps null: false
    end
  end
end
