# == Schema Information
#
# Table name: enrollments
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  section_id :integer
#  status     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Enrollment < ActiveRecord::Base
  belongs_to :user
  belongs_to :section
  
  validates :user_id, uniqueness: { scope: :section_id, message: "is already enrolled in that section" }
  
  ### status
  # 1 - interested (added to cart)
  # 2 - wait-listed
  # 3 - enrolled
  # 4 - course completed
  def status_abbr
    status = self.status
    return "Interested"  if status == 1
    return "Wait-Listed" if status == 2
    return "Enrolled"  if status == 3
    return "Completed" if status == 4
  end
  
  def add_student_to_section
    section = self.section
    if section.enrolled <= section.capacity - 1
      section.update_attribute(:enrolled, section.enrolled + 1)
      return true
    else
      section.update_attribute(:wait_listed, section.wait_listed + 1)
      return false
    end
  end
end
