# == Schema Information
#
# Table name: instructors
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  lecturer   :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  image      :string
#

class InstructorsController < ApplicationController
  def show
    @instructor = Instructor.find(params[:id])
  end
end
