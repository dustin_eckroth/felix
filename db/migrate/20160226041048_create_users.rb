class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :code
      t.string :first_name
      t.string :last_name
      t.string :email, index: true
      t.string :password_digest
      t.boolean :admin, default: false
      t.boolean :student, default: false
      t.boolean :employee, default: false
      t.boolean :instructor, default: false
      t.integer :total_credits
      t.float :gpa

      t.timestamps null: false
    end
  end
end
