# == Schema Information
#
# Table name: courses
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  prefix      :string
#  code        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  credits     :integer
#

module CoursesHelper
    def status_icon(course)
      return nil if params[:controller] != "users"
      
      course.sections.each do |section|
        if e = Enrollment.find_by(user_id: @user.id, section_id: section.id)
          return image_tag("check-icon.png", width: 20, height: 20, title: "Completed") if e.status == 4
          return image_tag("student-icon.png", width: 20, height: 20, title: "Enrolled") if e.status == 3
          return image_tag("warning-icon.png", width: 20, height: 20, title: "Wait-listed") if e.status == 2
        end
      end
      
      nil
      
    end
end
