# == Schema Information
#
# Table name: enrollments
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  section_id :integer
#  status     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EnrollmentsController < ApplicationController
  
  def new
  end
  
  def create
    @enrollment = Enrollment.new(user_id: session[:user_id], 
                        section_id: params[:section],
                        status: 1)
    if @enrollment.save
      flash[:success] = "Course #{@enrollment.section.course.abbr} added to cart."
    else
      flash[:danger] = "Already enrolled in course " + @enrollment.section.course.abbr
    end
    
    redirect_back_or courses_path
  end
  
  def destroy
    enrollment = Enrollment.find_by(user_id: session[:user_id], 
                                    section_id: params[:section])
    section = Section.find(params[:section])
    
    if enrollment.status == 2         # wait-listed
      section.update_attribute(:wait_listed, section.wait_listed - 1)
    elsif enrollment.status == 3      # enrolled
      section.update_attribute(:enrolled, section.enrolled - 1)
    end
    
    if enrollment.status == 4         # completed
      return false # don't delete completed courses from the record!
    end
    
    enrollment.delete
    redirect_back_or User.find(session[:user_id])
  end
end
