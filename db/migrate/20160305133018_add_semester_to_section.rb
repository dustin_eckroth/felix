class AddSemesterToSection < ActiveRecord::Migration
  def change
    add_column :sections, :semester, :string
    remove_column :blocks, :semester
  end
end
