# == Schema Information
#
# Table name: sections
#
#  id                  :integer          not null, primary key
#  course_id           :integer
#  instructor_id       :integer
#  code                :integer
#  capacity            :integer
#  enrolled            :integer
#  wait_listed         :integer
#  open_for_enrollment :boolean          default(FALSE)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  number              :integer
#  semester            :string
#

class Section < ActiveRecord::Base
  belongs_to :course
  belongs_to :instructor
  
  has_many   :blocks
  has_many   :enrollments
  
  has_many   :students, through: :enrollments, source: :users
  
  validates :course_id, presence: true
  validates :instructor_id, presence: true
  
  def block_start_dates
    string = String.new
    self.blocks.each do |block|
      string += block.start_date.to_s
      string += "<br>"
    end
    return string.html_safe
  end
  
  def block_end_dates
    string = String.new
    self.blocks.each do |block|
      string += block.end_date.to_s
      string += "<br>"
    end
    return string.html_safe
  end
  
  def block_days
    string = String.new
    
    self.blocks.each do |block|
      string += block.days
      string += "<br>"
    end
    return string.html_safe
  end
  
  def block_time_ranges
    string = String.new
    self.blocks.each do |block|
      if (block.start_time == "00:00") and (block.end_time == "00:00")
        string += "Hours Arranged"
        string += "<br>"
      else
        string += block.start_time.to_time.strftime("%I:%M %P")
        string += "-"
        string += block.end_time.to_time.strftime("%I:%M %P")
        string += "<br>"
      end
    end
    return string.html_safe
  end
  
  def block_locations
    string = String.new
    self.blocks.each do |block|
      string += CGI::escapeHTML(block.location)
      string += "<br>"
    end
    return string.html_safe
  end
  
  def padded_code
    code.to_s.rjust(2, '0')
  end
end
