# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  code            :integer
#  first_name      :string
#  last_name       :string
#  email           :string
#  password_digest :string
#  admin           :boolean          default(FALSE)
#  student         :boolean          default(FALSE)
#  employee        :boolean          default(FALSE)
#  instructor      :boolean          default(FALSE)
#  total_credits   :integer
#  gpa             :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    @interested_sections = @user.interested_sections
    @wait_listed_sections = @user.wait_listed_sections
    @enrolled_sections = @user.enrolled_sections
  end
  
  def enroll
    @user = User.find(session[:user_id])
    enrollments = @user.enrollments.where("status = ?", 1)
    
    enrollments.each do |e|
      if e.add_student_to_section
        e.update_attribute(:status, 3)
        flash[:success] = "Succesfully enrolled in courses."
      else
        e.update_attribute(:status, 2)
        flash[:warning] = "You have been wait-listed!"
      end
    end
    
    wait_listed_enrollments = @user.enrollments.where("status = ?", 2)
    
    wait_listed_enrollments.each do |w|
      section = w.section
      if section.enrollments < section.capacity
        w.update_attribute(:status, 3)
      end
    end

    redirect_back_or @user
  end
  
end
