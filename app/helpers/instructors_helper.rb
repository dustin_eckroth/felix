# == Schema Information
#
# Table name: instructors
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  lecturer   :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  image      :string
#

module InstructorsHelper
  
  def link_to_instructor(section)
    link_to section.instructor.last_name, instructor_path(section.instructor_id)
  end
end
