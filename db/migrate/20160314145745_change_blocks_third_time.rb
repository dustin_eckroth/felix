class ChangeBlocksThirdTime < ActiveRecord::Migration
  def change
    remove_column :blocks, :day
    add_column    :blocks, :days, :string
  end
end
