# == Schema Information
#
# Table name: buildings
#
#  id         :integer          not null, primary key
#  name       :string
#  abbr       :string
#  map        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BuildingsController < ApplicationController
  
  def show
    @building = Building.find(params[:id])
  end
  
  def index
    @buildings = Building.all
  end
  
end
