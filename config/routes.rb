# == Route Map
#
#        Prefix Verb   URI Pattern                   Controller#Action
#       courses GET    /courses(.:format)            courses#index
#               POST   /courses(.:format)            courses#create
#    new_course GET    /courses/new(.:format)        courses#new
#   edit_course GET    /courses/:id/edit(.:format)   courses#edit
#        course GET    /courses/:id(.:format)        courses#show
#               PATCH  /courses/:id(.:format)        courses#update
#               PUT    /courses/:id(.:format)        courses#update
#               DELETE /courses/:id(.:format)        courses#destroy
#               POST   /users/:id/enroll(.:format)   users#enroll
#         users GET    /users(.:format)              users#index
#               POST   /users(.:format)              users#create
#      new_user GET    /users/new(.:format)          users#new
#     edit_user GET    /users/:id/edit(.:format)     users#edit
#          user GET    /users/:id(.:format)          users#show
#               PATCH  /users/:id(.:format)          users#update
#               PUT    /users/:id(.:format)          users#update
#               DELETE /users/:id(.:format)          users#destroy
#    instructor GET    /instructors/:id(.:format)    instructors#show
#     buildings GET    /buildings(.:format)          buildings#index
#               POST   /buildings(.:format)          buildings#create
#  new_building GET    /buildings/new(.:format)      buildings#new
# edit_building GET    /buildings/:id/edit(.:format) buildings#edit
#      building GET    /buildings/:id(.:format)      buildings#show
#               PATCH  /buildings/:id(.:format)      buildings#update
#               PUT    /buildings/:id(.:format)      buildings#update
#               DELETE /buildings/:id(.:format)      buildings#destroy
#         rooms GET    /rooms(.:format)              rooms#index
#               POST   /rooms(.:format)              rooms#create
#      new_room GET    /rooms/new(.:format)          rooms#new
#     edit_room GET    /rooms/:id/edit(.:format)     rooms#edit
#          room GET    /rooms/:id(.:format)          rooms#show
#               PATCH  /rooms/:id(.:format)          rooms#update
#               PUT    /rooms/:id(.:format)          rooms#update
#               DELETE /rooms/:id(.:format)          rooms#destroy
#   enrollments POST   /enrollments(.:format)        enrollments#create
#          root GET    /                             static_pages#home
#         login GET    /login(.:format)              sessions#new
#               POST   /login(.:format)              sessions#create
#        logout DELETE /logout(.:format)             sessions#destroy
#

Rails.application.routes.draw do
  
  resources :courses
  resources :sections
  resources :blocks
  
  post 'user_enroll' => 'users#enroll'
  resources :users
  
  resources :instructors, only: [:show]
  resources :buildings
  resources :rooms
  
  resources :enrollments, only: [:create, :destroy]
  delete 'remove_course' => 'enrollments#destroy'
  
  root 'static_pages#home'
  
  get 'login'   => 'sessions#new'
  post 'login'  => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  
end
