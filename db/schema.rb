# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160314145745) do

  create_table "blocks", force: :cascade do |t|
    t.integer  "section_id"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "start_time"
    t.string   "end_time"
    t.integer  "room_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "days"
  end

  add_index "blocks", ["room_id"], name: "index_blocks_on_room_id"
  add_index "blocks", ["section_id"], name: "index_blocks_on_section_id"

  create_table "buildings", force: :cascade do |t|
    t.string   "name"
    t.string   "abbr"
    t.string   "map"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "prefix"
    t.string   "code"
    t.integer  "credits"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "section_id"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "enrollments", ["section_id"], name: "index_enrollments_on_section_id"
  add_index "enrollments", ["user_id"], name: "index_enrollments_on_user_id"

  create_table "instructors", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "lecturer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "image"
  end

  create_table "rooms", force: :cascade do |t|
    t.string   "number"
    t.integer  "building_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "rooms", ["building_id"], name: "index_rooms_on_building_id"

  create_table "sections", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "instructor_id"
    t.integer  "code"
    t.integer  "capacity"
    t.integer  "enrolled"
    t.integer  "wait_listed"
    t.boolean  "open_for_enrollment", default: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "number"
    t.string   "semester"
  end

  add_index "sections", ["course_id"], name: "index_sections_on_course_id"
  add_index "sections", ["instructor_id"], name: "index_sections_on_instructor_id"

  create_table "users", force: :cascade do |t|
    t.integer  "code"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.boolean  "admin",           default: false
    t.boolean  "student",         default: false
    t.boolean  "employee",        default: false
    t.boolean  "instructor",      default: false
    t.integer  "total_credits"
    t.float    "gpa"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email"

end
