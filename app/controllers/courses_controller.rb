# == Schema Information
#
# Table name: courses
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  prefix      :string
#  code        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  credits     :integer
#

class CoursesController < ApplicationController
  before_action :admin_user, only: [:edit, :update, :destroy, :new, :create]
  
  def index
    @courses = Course.search(params[:search]).order(:prefix).order(:code)
  end
  
  def new
    @course = Course.new
  end
  
  def create
    @course = Course.new(course_params)
    if @course.save
      flash[:success] = "Course created."
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def edit
    @course = Course.find(params[:id])
  end
  
  def update
    @course = Course.find(params[:id])
    if @course.update_attributes(course_params)
      flash[:success] = "Course updated."
      redirect_to @course
    else
      render 'edit'
    end
  end
  
  def show
    @course = Course.find(params[:id])
  end
  
  private
  
    def course_params
      params.require(:course).permit(:name, :prefix, :code, :description, :credits)
    end
    
    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless logged_in_user_is_admin?
    end
end
