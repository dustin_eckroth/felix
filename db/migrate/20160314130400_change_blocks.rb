
# Table name: blocks
#
#  id         :integer          not null, primary key
#  section_id :integer
#  start_date :date
#  end_date   :date
#  start_time :time
#  end_time   :time
#  monday     :boolean
#  tuesday    :boolean
#  wednesday  :boolean
#  thursday   :boolean
#  friday     :boolean
#  saturday   :boolean
#  sunday     :boolean
#  room_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ChangeBlocks < ActiveRecord::Migration
  def change
    remove_column :blocks, :monday
    remove_column :blocks, :tuesday
    remove_column :blocks, :wednesday
    remove_column :blocks, :thursday
    remove_column :blocks, :friday
    remove_column :blocks, :saturday
    remove_column :blocks, :sunday
    
    add_column :blocks, :day, :integer, after: :end_time
  end
end
