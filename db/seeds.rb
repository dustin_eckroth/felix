# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'csv'

Building.create!(name: "Electrical and Computer Engineering",
                    abbr: "EL&CO ENG", map: "ece-bldg.jpg")
Room.create!(number: 105, building_id: Building.last.id)
Room.create!(number: 123, building_id: Building.last.id)
Room.create!(number: 125, building_id: Building.last.id)
Room.create!(number: 205, building_id: Building.last.id)
Room.create!(number: 213, building_id: Building.last.id)
Room.create!(number: 237, building_id: Building.last.id)
Room.create!(number: 243, building_id: Building.last.id)

Building.create!(name: "E M Lebedeff Hall", abbr: "EM LEBDFF")
Room.create!(number: 370, building_id: Building.last.id)

Building.create!(name: "South Engineering", abbr: "SO ENG")
Room.create!(number: 116, building_id: Building.last.id)
Room.create!(number: 120, building_id: Building.last.id)

Building.create!(name: "STEM Classroom and Lab Building", abbr: "STEM")
Room.create!(number: 110, building_id: Building.last.id)
Room.create!(number: 112, building_id: Building.last.id)
Room.create!(number: 330, building_id: Building.last.id)

Building.create!(name: "West Dining Center", abbr: "WDNG")
Room.create!(number: "CLUS", building_id: Building.last.id)

Building.create!(name: "Sudro Hall", abbr: "SUDRO")
Room.create!(number: 21, building_id: Building.last.id)
Room.create!(number: 22, building_id: Building.last.id)
Room.create!(number: 27, building_id: Building.last.id)

Building.create!(name: "Construction Management Engineering", abbr: "CONST MGMT")
Room.create!(number: "AUDIT", building_id: Building.last.id)

Building.create!(name: "Civil and Industrial Engineering", abbr: "CI&IND ENG")
Room.create!(number: 101, building_id: Building.last.id)

CSV.foreach('/home/ubuntu/workspace/db/course_data.csv', headers: true) do |row|
  Course.create!(name: row[3], description: row[4],
                  prefix: row[0], code: row[1], credits: row[2])
end

CSV.foreach('/home/ubuntu/workspace/db/instructor_data.csv', headers: true) do |row|
  Instructor.create!(first_name: row[0], last_name: row[1], lecturer: row[2])
end

CSV.foreach('/home/ubuntu/workspace/db/section_data.csv', headers: true) do |row|
  Section.create!(course_id: Course.find_by(prefix: row[0], code: row[1]).id,
                    instructor_id: Instructor.find_by(last_name: row[3]).id,
                    code: row[2], number: row[4], semester: "SPR-16",
                    capacity: 30, enrolled: 0, wait_listed: 0)
  (0..4).each do |offset|
    if !row[6+offset*5].nil?
      if row[8+offset*5] == "N"
        day_string = "MWF"
      elsif row[8+offset*5] == "O"
        day_string = "TR" 
      else
        day_string = row[8+offset*5]
      end
      Block.create!(start_date: '2016-01-11', end_date: '2016-05-13',
                    start_time: row[6+offset*5], end_time: row[7+offset*5], 
                    days: day_string,
                    room_id: Room.find_by(number: row[9+offset*5]).id,
                    section_id: Section.last.id)
      if !row[31].nil? # override default start and end dates
        Block.last.update_attribute(:start_date, row[31])
        Block.last.update_attribute(:end_date, row[32])
      end
    end
  end
end

User.create!(code: 314159,
              first_name: "John",
              last_name: "Smith",
              email: "john.smith@example.com",
              password: "foobar",
              password_confirmation: "foobar",
              student: true)

User.create!(code: 314149,
              first_name: "John",
              last_name: "Jones",
              email: "john.jones@example.com",
              password: "foobar",
              password_confirmation: "foobar",
              student: true,
              admin: true)