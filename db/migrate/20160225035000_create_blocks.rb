class CreateBlocks < ActiveRecord::Migration
  def change
    create_table :blocks do |t|
      t.references :section, index: true, foreign_key: true
      t.date :start_date
      t.date :end_date
      t.time :start_time
      t.time :end_time
      t.boolean :monday
      t.boolean :tuesday
      t.boolean :wednesday
      t.boolean :thursday
      t.boolean :friday
      t.boolean :saturday
      t.boolean :sunday
      t.references :room, index: true, foreign_key: true
      t.string :semester

      t.timestamps null: false
    end
  end
end
