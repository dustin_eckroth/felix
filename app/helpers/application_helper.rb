module ApplicationHelper
  include CoursesHelper
  
  def starting_date
    date = Time.now if params[:date].nil?
    date ||= params[:date].to_date
    days_since_sunday = date.wday

    date - days_since_sunday.day
  end
end
